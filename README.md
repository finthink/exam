# An Economic Examination of Collateralization in Different Financial Markets

Collateralization is an essential element in the so-called plumbing of the financial system that is the Achilles' heel of global financial markets. It allows financial institutions to reduce economic capital and credit risk, free up lines of credit, and expand the range of counterparties. All of these factors contribute to the growth of financial markets. The benefits are broadly acknowledged and affect dealers and end users, as well as the financial system generally. 

The reason collateralization of financial derivatives and repos has become one of the most important and widespread credit risk mitigation techniques is that the Bankruptcy Code contains a series of “safe harbor” provisions to exempt these contracts from the “automatic stay”. The automatic stay prohibits the creditors from undertaking any act that threatens the debtor’s asset, while the safe harbor, a luxury, permits the creditors to terminate derivative and repo contracts with the debtor in bankruptcy and to seize the underlying collateral. This paper focuses on safe harbor contracts (e.g., derivatives and repos), but many of the points made are equally applicable to automatic stay contracts.

Financial derivatives can be categorized into three types. The first category is over-the-counter (OTC) derivatives, which are customized bilateral agreements. The second group is cleared derivatives, which are negotiated bilaterally but booked with a clearinghouse. Finally, the third type is exchange-traded/listed derivatives, which are executed over an exchange. The differences between the three types are described in detail on the International Swap Dealers Association (ISDA) website (see ISDA (2013)).

Under the new regulations (e.g., Dodd-Frank Wall Street Reform Act), certain ‘eligible’ OTC derivatives must be cleared with central counterparties (CCPs) (see Heller and Vause (2012), Pirrong (2011), etc.). Hull (2011) further recommends mandatory CCP clearing of all OTC derivatives. Meanwhile, Duffie and Zhu (2011) suggest a move toward the joint clearing of interest rate swaps and credit default swaps (CDS) in the same clearinghouse.

The posting of collateral is regulated by the Credit Support Annex (CSA). The CSA was originally designed for OTC derivatives, but more recently has been updated for cleared/listed derivatives. For this reason people in the financial industry often refer to collateralized contracts as CSA contracts and non-collateralized contracts as non-CSA contracts.

There are three types of collateralization: full, partial or over. Full-collateralization is a process where the posting of collateral is equal to the current mark-to-market (MTM) value. Partial/under-collateralization is a process where the posting of collateral is less than the current MTM value. Over-collateralization is a process where the posting of collateral is greater than the current MTM value.

From the perspective of collateral obligations, collateral arrangements can be unilateral or bilateral. In a unilateral arrangement, only one predefined counterparty has the right to call for collateral. Unilateral agreements are generally used when the other counterparty is much less creditworthy. In a bilateral arrangement, on the other hand, both counterparties have the right to call for collateral. 

Upon default and early termination, the values due under the ISDA Master Agreement are determined. These amounts are then netted and a single net payment is made. All of the collateral on hand would be available to satisfy this total amount, up to the full value of that collateral. In other words, the collateral to be posted is calculated on the basis of the aggregated value of the portfolio, but not on the basis of any individual transaction.

The use of collateral in financial markets has increased sharply over the past decade, yet analytical and empirical research on collateralization is relatively sparse. The effect of collateralization on financial contracts is an understudied area. Collateral management is often carried out in an ad-hoc manner, without reference to an analytical framework. Comparatively little research has been done to analytically and empirically assess the economic significance and implications of collateralization. Such a quantitative and empirical analysis is the primary contribution of this paper.

Due to the complexity of quantifying collateralization, previous studies seem to turn away from direct and detailed modeling of collateralization (see Fuijii and Takahahsi (2012)). For example, Johannes and Sundaresan (2007), and Fuijii and Takahahsi (2012) characterize collateralization via a cost-of-collateral instantaneous rate (or stochastic dividend or convenience yield). Piterbarg (2010) regards collateral as a regular asset in a portfolio and uses the replication approach to price collateralized contracts. All of the previous works focus on full-collateralization only.

We obtain the CSA data from two investment banks. The data show that only 8.92% of CSA counterparties are subject to unilateral collateralization, while the remaining 91.08% are bilaterally collateralized. The data also reveal that all contracts in OTC markets are partially collateralized due to the mechanics of CSAs, which allow for the existence of limited unsecured exposures and set minimum transfer amounts (MTAs), whereas all contracts in cleared/listed markets are over-collateralized as all CCPs/Exchanges require initial margins. Therefore, full-collateralization does not exist in the real world . The reason for the popularity of full-collateralization is its mathematical simplicity and tractability.

This article makes a theoretical and empirical contribution to the study of collateralization by addressing several essential questions concerning the posting of collateral. First, how does collateralization affect expected asset prices? To answer this question, we develop a comprehensive analytical framework for pricing financial contracts under different (partial/full/over and unilateral /bilateral) collateral arrangements in different (OTC/cleared/listed) markets.

In contrast to other collateralization models in current literature, we characterize a collateral process directly based on the fundamental principal and legal structure of the CSA agreement. A model is devised that allows for collateralization adhering to bankruptcy laws. As such, the model can back out differences in prices due to counterparty risk. This framework is very useful for valuing off-the-run or outstanding financial contracts subject to credit risk and collateralization, where the price quotes are not available. Given this model, we are able to explain credit-related spreads and provide an important tool for credit value adjustment (CVA).

Our theoretical analysis shows that collateralization can always improve recovery and reduce credit risk. If a contract is over-collateralized (e.g., a repo or cleared contract), its value is equal to the risk-free value. If a contract is partially collateralized (e.g., an OTC derivatives), its CSA value is less than the risk-free value but greater than the non-CSA risky value.

Second, how can the model be empirically verified? To achieve the verification goal, this paper empirically measures the effect of collateralization on pricing and compares it with model-implied prices. This calls for data on financial contracts that have different collateral arrangements but are similar otherwise. We use a unique interest rate swap contract data set from an investment bank for the empirical study, as interest rate swaps collectively account for around two-thirds of both the notional and market value of all outstanding derivatives.

ISDA mid-market swap rates quoted in the market are based on hypothetical counterparties of AA-rated quality or better. Dealers use this market rate as a reference when quoting an actual swap rate to a client and make adjustments based on many factors, such as credit risk, liquidity risk, funding cost, operational cost and expected profit, etc. Unlike most other studies, this study mainly concentrates the analysis on swap adjustments/premia related to credit risk and collateralization, which are to be made to the mid-market swap rates for real counterparties.

Prior research has primarily focused on the generic mid-market swap rates and results appear puzzling. Sorensen and Bollier (1994) believe that swap spreads (i.e., the difference between swap rates and par yields on similar maturity Treasuries) are partially determined by counterparty default risk. Whereas Duffie and Huang (1996), Hentschel and Smith (1997), Minton (1997) and Grinblatt (2001) find weak or no evidence of the impact of counterparty credit risk on swap spreads. Collin-Dufresne and Solnik (2001) and He (2001) further argue that many credit enhancement devices, e.g., collateralization, have essentially rendered swap contracts risk-free. Meanwhile, Duffie and Singleton (1997), and Liu, Longstaff and Mandell (2006) conclude that both credit and liquidity risks have an impact on swap spreads. Moreover, Feldhütter and Lando (2008) find that the liquidity factor is the largest component of swap spreads. It seems that there is no clear-cut answer yet regarding the relative contribution of the liquidity and credit factors. Maybe, the recently revealed LIBOR scandal can partially explain these conflicting findings.

Unlike the generic mid-market swap rates, swap premia are determined in a competitive market according to the basic principles of supply and demand. A client who wants to enter a swap contract first contacts a number of swap dealers and asks for a swap rate. After comparing all quotations, the client chooses the most competitive rate. A swap premium is supposed to cover operational, liquidity, funding, and credit costs as well as a profit margin. If the premium is too low, the dealer may lose money. If the premium is too high, the dealer may lose the competitive advantage.

Unfortunately, we do not know the detailed allocation of a swap premium, i.e., what percentage of the adjustment is charged for each factor. Thus, a direct empirical verification is impossible.

To circumvent this difficulty, this article uses an indirect process to verify the model empirically. We define a swap premium spread as the difference between the swap premia of two collateralized swap contracts that have exactly the same terms and conditions but are traded with different counterparties under different collateral agreements. We reasonably believe that if two contracts are identical except counterparties, the premium spread should reflect the difference between two counterparties’ unsecured credit risks only, as all other risks and costs are identical. 

Empirically, we find quite a number of CSA swap pairs in the data, where the two contracts in each pair have different counterparties but are otherwise the same. The test results demonstrate that the model-implied swap premium spreads are very close to the market swap premium spreads, indicating that the model is quite accurate. 

Third, what are the effects of counterparty risk and collateralization, alone or combined, on swap premium spreads? We first study the marginal impact of counterparty risk on spreads. CDS premia (the prices of insuring against a firm’s default) theoretically reflect the credit risk of the firm. Presumably, differences in CDS premia should be largely attributable to differences in counterparty risk. We estimate a regression model where market swap premium spreads are used as the dependent variable and differences between CDS premia as the explanatory variable. The estimation results show that the adjusted   is 0.7472, implying that approximately 75% of market premium spreads can be explained by CDS spreads. In other words, counterparty risk alone can provide a good but not overwhelming prediction on spreads. 

Next, we assess the joint effect of both counterparty risk and collateralization on premium spreads. Since the model-generated swap premium spreads take into account both counterparty risk and collateralization, we present another regression model where the market swap premium spreads are regressed on the implied swap premium spreads. The estimation results show that the constant term is insignificantly different from zero; the slope coefficient is close to 1 and the adjusted   is very high. This suggests that the implied premium spreads explains nearly all of the market premium spreads.

Finally, how does collateralization impact pricing in different markets? Our proprietary data reveal that cleared swaps have dramatically increased since 2011, reflecting the financial institutions’ compliance to regulatory requirements. We find evidence that the economical determination of swap rates in cleared markets is the same as that in OTC markets, as all clearinghouses claim that cleared derivatives would replicate OTC derivatives, and promise that transactions through the clearinghouses would be economically equivalent to similar transactions handled in OTC markets. 

Although the practice recommended by CCPs is popular in the market, in which derivatives are continuously negotiated over-the-counter as usual but cleared and settled through clearinghouses, some market participants cast doubt on CCPs’ economic equivalence claim. They find that cleared contracts have actually significant differences when compared with OTC trades. Some firms even file legal action against the clearinghouses, and accuse them of fraudulently inducing the firms to enter into cleared derivatives on the premise the contracts would be economically equivalent to OTC contracts (see Pengelly (2011)). 

In fact, our study shows that there are many differences between cleared markets and OTC markets. After much discussion, we come to the conclusion that cleared derivatives are not economically equivalent to their OTC counterparts. These discussions may be of interest to regulators, academics and practitioners.

References

AFME, ICMA, and ISDA, 2011, “The impact of derivative collateral policies of European sovereigns and resulting Basel III capital issues.”

Collin-Dufresne, P. and B. Solnik, 2001, “On the term structure of default premia in the swap and LIBOR markets,” Journal of Finance 56, 1095-1115.

Cont, R. and T. Kokholm, 2011, “Central clearing of OTC derivatives bilateral vs. multilateral netting,” Working paper.

Cont, R., R. Mondescu, and Y. Yu, 2011, “Central clearing of interest rate swaps: a comparison of offerings,” Working Paper.

Duffie, D. and M. Huang, 1996, “Swap rates and credit quality,” Journal of Finance, 51, 921-949.

Duffie, D., and K.J. Singleton, 1999, “Modeling term structure of defaultable bonds,” Review of Financial Studies, 12, 687-720.

Duffie, D., and H. Zhu, 2011, “Does a central clearing counterparty reduce counterparty risk?” Review of Asset Pricing Studies, 74-95.

Edwards, F. and E. Morrison, 2005, “Derivatives and the bankruptcy code: why the special treatment?” Yale Journal on Regulation, 22, 101-133.

Feldhutter, P. and D. Lando, 2008, “Decomposing swap spreads,” Journal of Financial Economics,” 88 (2), 375-405.

FinPricing, 2018, Swap Data, https://finpricing.com/lib/FiBond.html

European Central Bank (ECB), 2009, “Credit default swaps and counterparty risk.”

Fuijii, M. and A. Takahahsi, “Collateralized CDS and default dependence – Implications for the central clearing,” Forthcoming, Journal of Credit Risk.

Garlson, D., 1991-1992, “Secured creditors and the eely character of bankruptcy valuations,” 41 American University Law Review, 63, 63-106.

Grinblatt, M., 2001, “An analytic solution for interest rate swap spreads,” Review of International Finance, 2, 113-149.

He, H., 2001, “Modeling Term Structures of Swap Spreads,” working paper, Yale School of Management, Yale University.

Heller, D. and N. Vause, 2012, “Collateral requirements for mandatory central clearing of over-the-counter derivatives,” BIS Working Papers, No 373.

Hentschel, L. and C. W. Smith, Jr., 1997, “Risks in Derivatives Markets: Implications for the Insurance Industry." Journal of Risk and Insurance, 64, 323-346.

Hull, J., 2010, “OTC derivatives and central clearing: can all transactions be cleared?” Financial Stability Review, 14, 71-80.

ISDA, 2013, Product Descriptions and Frequently Asked Questions (1. Major derivative categories; 2. How do cleared derivatives differ from OTC derivatives?), http://www.isda.org/educat/faqs.html

ISDA, 2012, “ISDA margin survey 2012.” 

Johannes, M. and S. Sundaresan, 2007, “The impact of collateralization on swap rates,” Journal of Finance, 62, 383-410.

J.P. Morgan, 2001, “Par credit default swap spread approximation from default probabilities.”

Liu, J., F. Longstaff, and R. Mandell, 2006, "The Market Price of Risk in Interest Rate Swaps: The Roles of Default and Liquidity Risks," The Journal of Business, vol. 79, pages 2337-2360.

McPartland, J., P. Khorrami, R. Ranjan, and K. Wells, 2011, “An algorithm to estimate the total amount of collateral required before and after financial reform initiatives in various, regulatory jurisdictions,” Federal Reserve Bank of Chicago.

Minton, B., 1997, “An Empirical Examination of Basic Valuation Models for Plain Vanilla U.S. Interest Rate Swaps." Journal of Financial Economics, 251-277.

Moody’s Investor’s Service, 2000, “Historical default rates of corporate bond issuers, 1920-99,”

O’Kane, D. and S. Turnbull, 2003, “Valuation of credit default swaps,” Fixed Income Quantitative Credit Research, Lehman Brothers, QCR Quarterly, 2003 Q1/Q2, 1-19.

Pengelly, M., 2011, “Margin minutiae at issue in Jefferies v IDCG suit,” Risk magazine, 02 Nov 2011

Pirrong, C., 2011, “The economics of central clearing: theory and practice,” ISDA Discussion Papers Series.

Piterbarg, V., 2010, “Funding beyond discounting: collateral agreements and derivatives pricing,” Risk Magazine, 2010 (2), 97-102.

Routh, R. and M. Douglas, 2005, “Default interest payable to oversecured creditor subject to reasonableness limitation,” ABF Journal, Vol. 4, No. 3.

Singh, M., 2010, “Collateral, netting and systemic risk in the OTC derivatives market,” IMF Working Paper No. 10/99.

Sorensen, E. and T. Bollier, 1994, “Pricing swap default risk,” Financial Analysts Journal, 50, 23-33.

Xiao, T., 2011, “An efficient lattice algorithm for the LIBOR market model,” Journal of Derivatives, 19 (1): 25-40.
